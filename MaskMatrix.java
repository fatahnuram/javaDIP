package javaDIP;

public class MaskMatrix {
    private final int xy;
    private int[][] element;
    
    MaskMatrix(int xy, int[] element){
        this.xy = xy;
        int count = 0;
        this.element = new int[this.xy][this.xy];
        
        for(int i=0; i<xy; i++){
            for(int j=0; j<xy; j++){
                this.element[i][j] = element[count++];
            }
        }
    }
    
    public int[][] getMatrix(){
        return element;
    }
    
    public int getElement(int x, int y){
        return element[x][y];
    }
    
    public int getSize(){
        return xy;
    }
    
    public void setElement(int x, int y, int element){
        this.element[x][y] = element;
    }
    
    public void print(){
        for(int i=0; i<xy; i++){
            for(int j=0; j<xy; j++){
                System.out.print(element[i][j] + " ");
            }
            
            System.out.println();
        }
    }
}