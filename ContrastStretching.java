package javaDIP;

import java.awt.*;
import java.awt.image.*;
import java.io.*;
import javax.swing.*;
import javax.imageio.*;

public class ContrastStretching{
    // variable declarations
    private BufferedImage img;
    
    // constructor
    public ContrastStretching(String file){
        try{
            img = ImageIO.read(new File(file));
        }catch(IOException e){
            e.printStackTrace();
        }
    }
    
    // method details to get buffered image
    public BufferedImage getBufferedImage(){
        return img;
    }
    
    // method details to show image using javax
    public void showImage(BufferedImage imgSrc){
        ImageIcon imgIcon;
        JLabel label;
        
        imgIcon = new ImageIcon(imgSrc);
        label = new JLabel();
        label.setIcon(imgIcon);
        
        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame frame = new JFrame("Contrast Stretching");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(label, BorderLayout.CENTER);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
    
    // method details to show image in grayscale
    public void showImageGrayScale(BufferedImage imgSrc){
        BufferedImage imgGray = new BufferedImage(imgSrc.getWidth(), 
                imgSrc.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        Graphics g = imgGray.getGraphics();
        g.drawImage(imgSrc, 0, 0, null);
        g.dispose();
        this.showImage(imgGray);
    }
    
    // method details to get buffered image in grayscale
    public BufferedImage getImageGrayScale(BufferedImage imgSrc){
        BufferedImage imgGray = new BufferedImage(imgSrc.getWidth(), 
                imgSrc.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        Graphics g = imgGray.getGraphics();
        g.drawImage(imgSrc, 0, 0, null);
        g.dispose();
        return imgGray;
    }
    
    // method details to show pixel value in each coordinate
    public void showPixelValue(BufferedImage imgSrc){
        for(int i=0; i<imgSrc.getHeight(); i++){
            for(int j=0; j<imgSrc.getWidth(); j++){
                Color c = new Color(imgSrc.getRGB(j,i));
                System.out.println("Pixel at (" + i + "," + j + "): " +
                        "Red: " + c.getRed() + "; " +
                        "Green: " + c.getGreen() + "; " +
                        "Blue: " + c.getBlue() + "; " +
                        "Alpha: " + c.getAlpha() + ";");
            }
        }
    }
    
    // method details to show pixel value in grayscale mode
    public void showPixelValueGrayScale(BufferedImage imgSrc){
        for(int i=0; i<imgSrc.getHeight(); i++){
            for(int j=0; j<imgSrc.getWidth(); j++){
                int pixelValue = imgSrc.getData().getSample(j, i, 0);
                System.out.println("Pixel at (" + i + "," + j + "); " +
                        "Value: " + pixelValue + ";");
            }
        }
    }
    
    // method details to convert image to grayscal and show to screen
    public void toGrayScale(BufferedImage imgSrc){
        for(int i=0; i<imgSrc.getHeight(); i++){
            for(int j=0; j<imgSrc.getWidth(); j++){
                int rgb = imgSrc.getRGB(j, i);
                int red = (rgb >> 16) & 0xFF;
                int green = (rgb >> 8) & 0xFF;
                int blue = (rgb & 0xFF);
                int sum = (red + green + blue) / 3;
                int grayLevel = (sum << 16) + (sum << 8) + sum;
                
                imgSrc.setRGB(j, i, grayLevel);
            }
        }
        
        this.showImage(imgSrc);
    }
    
    // method details to get value frequency on grayscale image
    public int[] getFreqGrayScale(BufferedImage imgSrc){
        int[] freq = new int[256];
        
        for(int i=0; i<imgSrc.getHeight(); i++){
            for(int j=0; j<imgSrc.getWidth(); j++){
                int pixel = imgSrc.getData().getSample(j, i, 0);
                freq[pixel]++;
            }
        }
        
        return freq;
    }
    
    // method details to print grayscale frequency
    public void showFreqGrayScale(BufferedImage imgSrc){
        int[] freq = getFreqGrayScale(imgSrc);
        
        for(int k=0; k<256; k++){
            System.out.println("Value[" + k + "]: " + freq[k]);
        }
    }
    
    // method details to show histogram of grayscale image
    public void makeHistogramGrayScale(BufferedImage imgSrc){
        int[] freq = getFreqGrayScale(imgSrc);
        int[] histo = new int[256];
        
        for(int i=0; i<256; i++){
            System.out.print("Val[" + i + "]: ");
            
            for(int j=0; j<freq[i]/100; j++){
                System.out.print("#");
            }
            
            System.out.println();
        }
    }
    
    // show pixel statistics of image
    public void showStatistics(BufferedImage imgSrc){
        int min = 256;
        int max = -1;
        long sum = 0;
        double avg;
        double sd;
        double sumPixel2 = 0;
        double avgPixel2 = 0;
        
        for(int i=0; i<imgSrc.getHeight(); i++){
            for(int j=0; j<imgSrc.getWidth(); j++){
                int pixel = imgSrc.getData().getSample(j, i, 0);
                
                sum += pixel;
                
                if(pixel < min){
                    min = pixel;
                }
                if(pixel > max){
                    max = pixel;
                }
            }
        }
        
        avg = sum / (imgSrc.getHeight() * imgSrc.getWidth());
        
        for(int i=0; i<imgSrc.getHeight(); i++){
            for(int j=0; j<imgSrc.getWidth(); j++){
                int pixel = imgSrc.getData().getSample(j, i, 0);
                
                sumPixel2 += (pixel - avg) * (pixel - avg);
            }
        }
        
        avgPixel2 = sumPixel2 / (imgSrc.getHeight() * imgSrc.getWidth());
        
        sd = Math.sqrt(avgPixel2);
        
        System.out.println("Min value pixel: " + min);
        System.out.println("Max value pixel: " + max);
        System.out.println("Average value pixel: " + avg);
        System.out.println("Standard deviation: " + sd);
    }
    
    // method details of contrast stretching
    public BufferedImage doContrastStretching(BufferedImage imgSrc){
        BufferedImage imgSrc2 = imgSrc;
        
        for(int i=0; i<imgSrc2.getHeight(); i++){
            for(int j=0; j<imgSrc2.getWidth(); j++){
                int pixel = imgSrc2.getData().getSample(j, i, 0);
                
                if(pixel <= 125){
                    pixel = (int) (pixel * 0.6);
                }else{
                    if(pixel >= 175){
                        pixel = (int) ((pixel - 175) / 16 + 250);
                    }else{
                        pixel = (int) (175 * (pixel - 125) / 50 + 75);
                    }
                }
                
                if(pixel > 255) pixel = 255;
                if(pixel < 0) pixel = 0;
                
                imgSrc2.getRaster().setSample(j, i, 0, pixel);
            }
        }
        
        return imgSrc2;
    }
    
    // method details to show image before and after process
    public void showDifferences(BufferedImage imgSrcA, BufferedImage imgSrcB){
        JLabel labelA = new JLabel(new ImageIcon(imgSrcA));
        JLabel labelB = new JLabel(new ImageIcon(imgSrcB));
        
        JPanel panel = new JPanel();
        
        panel.add(labelA);
        panel.add(labelB);
        
        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame frame = new JFrame("Contrast Stretching");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(panel, BorderLayout.CENTER);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
