package javaDIP;

import java.awt.*;
import java.awt.image.*;
import java.io.*;
import javax.swing.*;
import javax.imageio.*;

public class Tes implements Runnable{
    private BufferedImage img;
    private String outputFile;
    
    public Tes(String input, String output){
        try{
            img = ImageIO.read(new File(input));
        }catch(IOException e){
            e.printStackTrace();
        }

        img = this.getImageGrayScale(img);

        outputFile = output;
    }

    public void saveToFile(){
        try{
            ImageIO.write(img, "jpg", new File(outputFile));
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public BufferedImage getBufferedImage(){
        return img;
    }
    
    public BufferedImage getImageGrayScale(BufferedImage imgSrc){
        BufferedImage imgGray = new BufferedImage(imgSrc.getWidth(), 
                imgSrc.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        Graphics g = imgGray.getGraphics();
        g.drawImage(imgSrc, 0, 0, null);
        g.dispose();
        return imgGray;
    }

    public void doZeroPixel(){
        for(int i=0; i<img.getWidth(); i++){
            img.getRaster().setSample(i,img.getHeight()-1,0,0);
        }
    }

    @Override
    public void run(){
        this.doZeroPixel();
        this.saveToFile();
    }
}