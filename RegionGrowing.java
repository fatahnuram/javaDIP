package javaDIP;

import java.util.*;
import java.awt.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;

public class RegionGrowing implements Runnable{
	private BufferedImage img;
	private BufferedImage imgFinal;
	private Stack<int[]> stack = new Stack<>();
	private int[] seed = new int[2];
	private boolean[][] flag, inStack;
	private String outputFile;

	public RegionGrowing(String input, String output){
		try{
			img = ImageIO.read(new File(input));
		}catch(Exception e){
			e.printStackTrace();
		}

		img = this.getImageGrayScale(img);
		imgFinal = img;

		this.initFlags();
		this.generateSeed();

		outputFile = output;
	}

	private int getTotalPixel(){
		return img.getHeight() * img.getWidth();
	}

	private void generateSeed(){
		seed[0] = (int) (Math.random() * img.getHeight());
		seed[1] = (int) (Math.random() * img.getWidth());
	}

	public void showSeed(){
		System.out.println("Seed: (" + seed[0] + ", " + seed[1] + ")");
	}

	private void showStatus(int row, int column){
		System.out.println("Now at point: (" + row + ", " + column + ")");
	}

	private void initFlags(){
		flag = new boolean[img.getHeight()][img.getWidth()];

		for(boolean[] row : flag){
			for(boolean cell : row){
				cell = false;
			}
		}

		inStack = flag;
	}

	private int determinePixelValue(int[] src){
		if(this.getPixelValue(src[0],src[1]) < 128) return 255;
		else return 0;
	}

	private void setPixelValue(int row, int column, int value){
		imgFinal.getRaster().setSample(column,row,0,value);
	}

	private void setFlag(int row, int column){
		flag[row][column] = true;
	}

	private void setInStack(int row, int column){
		inStack[row][column] = true;
	}

	private boolean isSetFlag(int row, int column){
		return flag[row][column];
	}

	private boolean isInStack(int row, int column){
		return inStack[row][column];
	}

	private int getPixelValue(int row, int column){
		return img.getData().getSample(column, row, 0);
	}

	private BufferedImage getImageGrayScale(BufferedImage imgSrc){
        BufferedImage imgGray = new BufferedImage(imgSrc.getWidth(), 
                imgSrc.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        Graphics g = imgGray.getGraphics();
        g.drawImage(imgSrc, 0, 0, null);
        g.dispose();

        return imgGray;
    }

	public void doRegionGrowing(){
		int[] temp = new int[2];
		int[] index = new int[2];
		stack.push(seed);
		int pixel = this.determinePixelValue(seed);

		while(!stack.empty()){
			temp = stack.pop();
			this.showStatus(temp[0],temp[1]);

			//check left neighbor
			if((Math.abs(getPixelValue(temp[0],temp[1]-1)-getPixelValue(temp[0],temp[1])))<255 &&
					!isSetFlag(temp[0],temp[1]-1) && (temp[1]-1)>0 && !isInStack(temp[0],temp[1]-1)){
				index[0] = temp[0];
				index[1] = temp[1]-1;
				stack.push(index);
				this.showStatus(index[0],index[1]);
				this.setInStack(index[0],index[1]);
			}

			//check bottom neighbor
			if((Math.abs(getPixelValue(temp[0]+1,temp[1])-getPixelValue(temp[0],temp[1])))<255 &&
					!isSetFlag(temp[0]+1,temp[1]) && (temp[0]+1)<img.getHeight()-1 && !isInStack(temp[0]+1,temp[1])){
				index[0] = temp[0]+1;
				index[1] = temp[1];
				stack.push(index);
				this.showStatus(index[0],index[1]);
				this.setInStack(index[0],index[1]);
			}

			//check right neighbor
			if((Math.abs(getPixelValue(temp[0],temp[1]+1)-getPixelValue(temp[0],temp[1])))<255 &&
					!isSetFlag(temp[0],temp[1]+1) && (temp[1]+1)<img.getWidth()-1 && !isInStack(temp[0],temp[1]+1)){
				index[0] = temp[0];
				index[1] = temp[1]+1;
				stack.push(index);
				this.showStatus(index[0],index[1]);
				this.setInStack(index[0],index[1]);
			}

			//check top neighbor
			if((Math.abs(getPixelValue(temp[0]-1,temp[1])-getPixelValue(temp[0],temp[1])))<255 &&
					!isSetFlag(temp[0]-1,temp[1]) && (temp[0]-1)>0 && !isInStack(temp[0]-1,temp[1])){
				index[0] = temp[0]-1;
				index[1] = temp[1];
				stack.push(index);
				this.showStatus(index[0],index[1]);
				this.setInStack(index[0],index[1]);
			}

			setFlag(temp[0],temp[1]);
			setPixelValue(temp[0],temp[1],0);
		}
	}

	public void saveToFile(){
		try{
			ImageIO.write(imgFinal, "jpg", new File(outputFile));
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void run(){
		this.doRegionGrowing();
		this.saveToFile();
		this.showSeed();
	}
}