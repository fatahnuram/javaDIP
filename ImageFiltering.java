package javaDIP;

import java.awt.*;
import java.awt.image.*;
import java.io.*;
import javax.swing.*;
import javax.imageio.*;

public class ImageFiltering {
    private BufferedImage img;
    
    public ImageFiltering(String file){
        try{
            img = ImageIO.read(new File(file));
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public BufferedImage getBufferedImage(){
        return img;
    }
    
    public void showImage(BufferedImage imgSrc){
        ImageIcon imgIcon;
        JLabel label;
        
        imgIcon = new ImageIcon(imgSrc);
        label = new JLabel();
        label.setIcon(imgIcon);
        
        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame frame = new JFrame("Image Filtering");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(label, BorderLayout.CENTER);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
    
    public BufferedImage doImageFilter(BufferedImage imgSrc, MaskMatrix mask){
        int start = (mask.getSize()-1)/2;
        int center = start+1;
        int counter = 0;
        BufferedImage img = new BufferedImage(imgSrc.getWidth()-start,
                imgSrc.getHeight()-start, BufferedImage.TYPE_BYTE_GRAY);
        
        for(int i=start; i<imgSrc.getHeight()-start; i++){
            for(int j=start; j<imgSrc.getWidth()-start; j++){
                int result = 0;
                int temp = 0;
                for(int k=0; k<mask.getSize(); k++){
                    for(int l=0; l<mask.getSize(); l++){
                        temp = mask.getElement(k,l) *
                                imgSrc.getData().getSample(j+k-1, i+l-1, 0);
                        result += temp;
                        System.out.println(counter++);
                    }
                }
                
                if(result > 255) result = 255;
                if(result < 0) result = 0;
                setFilteredPixel(img, j-start, i-start, result);
            }
        }
        
        return img;
    }
    
    public void setFilteredPixel(BufferedImage imgSrc, int x, int y, int value){
        imgSrc.getRaster().setSample(x, y, 0, value);
    }
    
    public void saveImage(BufferedImage imgSrc, String format, String path){
        try{
            ImageIO.write(imgSrc, format, new File(path));
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
