package javaDIP;

import java.awt.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;

public class Otsu implements Runnable{
  private BufferedImage img;
  private BufferedImage imgFinal;
  private int[] pixelCounter = new int[256];
  private double[] probabilityPixel = new double[256];
  private double[] classProbabilityA = new double[256];
  private double[] classProbabilityB = new double[256];
  private double[] classMeanA = new double[256];
  private double[] classMeanB = new double[256];
  private double[] classVariance = new double[256];
  private double imgMean;
  private int optimumThreshold;
  private String outputFile;

  public Otsu (String input, String output){
    try{
      img = ImageIO.read(new File(input));
    }catch(Exception e){
      e.printStackTrace();
    }

    for(int i=0; i<256; i++){
      pixelCounter[i] = 0;
      classProbabilityA[i] = classProbabilityB[i] = 0;
      classMeanA[i] = classMeanB[i] = 0;
      /*classVariance[i] = */probabilityPixel[i]= 0;
    }

    outputFile = output;
  }

  public BufferedImage getBufferedImage(){
    return img;
  }

  public int getThreshold(){
    return optimumThreshold;
  }

  public int getTotalPixel(){
    return img.getHeight()*img.getWidth();
  }

  public int getPixelCounter(int a){
    return pixelCounter[a];
  }

  public void saveToFile(){
    try{
      ImageIO.write(imgFinal, "jpg", new File(outputFile));
    }catch(Exception e){
      e.printStackTrace();
    }
  }

  private void countPixelsCount(){
    for(int i=0; i<img.getHeight(); i++){
      for(int j=0; j<img.getWidth(); j++){
        int val = img.getData().getSample(j,i,0);
        pixelCounter[val]++;
      }
    }
  }

  private void countProbability(){
    for(int i=0; i<256; i++){
      probabilityPixel[i] = (double) this.getPixelCounter(i)/this.getTotalPixel();
    }
  }

  private void countClassProbability(){
    for(int i=0; i<256; i++){
      for(int j=0; j<=i; j++){
        classProbabilityA[i] += probabilityPixel[j];
      }

      for(int j=i+1; j<256; j++){
        classProbabilityB[i] += probabilityPixel[j];
      }
    }
  }

  private void countClassMean(){
    for(int i=0; i<256; i++){
      for(int j=0; j<=i; j++){
        classMeanA[i] += j*probabilityPixel[j];
      }
      if(classProbabilityA[i] != 0) classMeanA[i] /= classProbabilityA[i];
      else classMeanA[i] = 0;

      for(int j=i+1; j<256; j++){
        classMeanB[i] += j*probabilityPixel[j];
      }
      if(classProbabilityB[i] != 0) classMeanB[i] /= classProbabilityB[i];
      else classMeanB[i] = 0;
    }
  }

  private void countImageMean(){
    imgMean = 0;

    for(int i=0; i<256; i++){
      imgMean += i*probabilityPixel[i];
    }
  }

  @Override
  public void run(){
    this.countPixelsCount();
    this.countProbability();
    this.countClassProbability();
    this.countClassMean();
    this.countImageMean();
    this.findThreshold();
    this.applyThreshold();
    this.saveToFile();
    this.tes();
  }

  public void findThreshold(){
    int minIndex = 0;
    double minVariance = 1000;

    for(int i=0; i<256; i++){
      classVariance[i] = classProbabilityA[i]*(classMeanA[i]-imgMean)*(classMeanA[i]-imgMean) +
            classProbabilityB[i]*(classMeanB[i]-imgMean)*(classMeanB[i]-imgMean);

      if(i == 0){
        minIndex = i;
        minVariance = classVariance[i];
      }

      if(classVariance[i] <= minVariance){
        minIndex = i;
        minVariance = classVariance[i];
      }
    }

    optimumThreshold = minIndex;
  }

  public void applyThreshold(){
    BufferedImage img2 = img;

    for(int i=0; i<img2.getHeight(); i++){
      for(int j=0; j<img2.getWidth(); j++){
        if(img2.getData().getSample(j,i,0) < optimumThreshold){
          img2.getRaster().setSample(j,i,0,0);
        }else{
          img2.getRaster().setSample(j,i,0,255);
        }
      }
    }

    imgFinal = new BufferedImage(img2.getWidth(), img2.getHeight(),
      BufferedImage.TYPE_BYTE_BINARY);
    Graphics g = imgFinal.getGraphics();
    g.drawImage(img2, 0, 0, null);
    g.dispose();
  }

  public void tes(){
    System.out.println("Hasil = ");

    for(int i=0; i<imgFinal.getHeight(); i++){
      for(int j=0; j<imgFinal.getWidth(); j++){
        System.out.println(imgFinal.getData().getSample(j,i,0));
      }
    }

    System.out.println("optimumThreshold = " + optimumThreshold);

    System.out.println("pixelCounter = ");
    for(int element : pixelCounter){
      System.out.println(element);
    }

    System.out.println("probabilityPixel = ");
    for(double element : probabilityPixel){
      System.out.println(element);
    }

    System.out.println("classProbabilityA = ");
    for(double element : classProbabilityA){
      System.out.println(element);
    }

    System.out.println("classProbabilityB = ");
    for(double element : classProbabilityB){
      System.out.println(element);
    }

    System.out.println("classMeanA = ");
    for(double element : classMeanA){
      System.out.println(element);
    }

    System.out.println("classMeanB = ");
    for(double element : classMeanB){
      System.out.println(element);
    }

    System.out.println("classVariance = ");
    for(double element : classVariance){
      System.out.println(element);
    }

    System.out.println("imgMean = " + imgMean);
  }
}
